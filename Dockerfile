
FROM maven:3.8.4-jdk-8 as builder

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN mvn clean package

FROM openjdk:8-jre-alpine

COPY --from=builder /usr/src/app/target/*.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["java"]

CMD ["-jar", "/app.jar"]
